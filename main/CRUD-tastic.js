/* global $ PouchDB */

// smelly globals time
var out;

$(() => {
  out = $('#console');

  // DB Handle
  var port = 5000; // you may have to change this
  var options = {
    auth: {
      //username: 'testing',
      //password: '123abc'
      username: 'DEFAULT_USER',
      password: 'DEFAULT_PASSWORD'
    }
  };

  checkDefaults(options);

  var music = new PouchDB(`http://localhost:${port}/music`, options);
  //var user = new PouchDB(`http://localhost:${port}/users`, options);
  //var playlist = new PouchDB(`http://localhost:${port}/playlists`, options);

  // Some quick and dirty jquery

  clickEvents();

  function clickEvents() {
    // Info & Create
    $('#info-and-create').click(() => {
      music.info().finally(output);
      //user.info().finally(output);
      //playlist.info().finally(output);
    });

    // Add Artist
    $('#add-artist').click(() => {
      let artist = $('#artist_name').val();
      let song = $('#song_name').val();

      getArtist(artist).then((doc) => {
        output(doc);
        let addingSong = addSong(doc, song);
        if (addingSong) {
          addingSong.finally(output);
        }
      }).catch((err) => {
        if (err.status == 404 || err.status == 412) {
          addArtist(artist, [song]).finally(output);
        } else {
          output(err);
        }
      });
    });

    $('#list-artists').click(() => {
      getArtists(true);
    });

    $('#list-songs').click(() => {
      let artist = $('#songs_artist_name').val();
      listSongs(artist);
    });

    $('#rename-artist').click(() => {
      let oldName = $('#udpate_old_artist_name').val();
      let newName = $('#update_new_artist_name').val();
      renameArtist(oldName, newName);
    });

    $('#delete-artist').click(() => {
      let artist = $('#delete_artist_name').val();
      deleteArtist(artist);
    });

  }

  // Fun Functions™
  function renameArtist(oldName, newName) {
    getArtist(oldName).then((doc) => {
      doc.artist = newName;
      return music.put(doc).finally(output);
    }).catch(output);
  }

  function deleteArtist(artist) {
    getArtist(artist).then((doc) => {
      music.remove(doc).finally(output);
    }).catch(output);

  }

  function listSongs(artist) {
    getArtist(artist).then((doc) => {
      output(`## ${artist} \n### Songs \n - ${doc.songs.join('\n - ')}`);
    }).catch(output);
  }

  function getArtists(docs = false) {
    let options = {};
    if (docs) {
      options = {
        include_docs: true
      };
    }
    return music.allDocs(options).then((ret) => {
      let artists = ret.rows.map((row) => {
        return row.id + (row.id == row.doc.artist ? '' :  ` AKA: ${row.doc.artist}`);
      });
      output(`### Artists \n - ${artists.join('\n - ')}`);
    });
  }

  // Get Artist
  function getArtist(name) {
    return music.get(name);
  }

  // Add Artist
  function addArtist(name, songs = []) {
    return music.put({
      _id: name,
      artist: name,
      songs: songs
    });
  }

  // Add Song
  function addSong(doc, name) {
    if (doc.songs.indexOf(name) === -1) {
      doc.songs.push(name);
      return music.put(doc);
    } else {
      return false;
    }
  }

}); // jquery ready $(()=>{})


$('#clear-console').click(() => {
  out.html('');
});

function output(msg) {
  let html = out.html();
  if (typeof msg == 'object') {
    msg = JSON.stringify(msg);
  }
  out.html(`${html}${msg}<br><br>`);
}

function checkDefaults(options) {
  if (options.auth.username == 'DEFAULT_USER' || options.auth.password == 'DEFAULT_PASSWORD') {
    output('Change your username and password from DEFAULT_USER and/or DEFAULT_PASSWORD! in CRUD-tastic.js\n' +
      'Also create a user in Fauxton UI (link @ bottom right) if you have not yet done so.');
  }
}

// Inspired by: https://stackoverflow.com/questions/35999072/what-is-the-equivalent-of-bluebird-promise-finally-in-native-es6-promises
Promise.prototype.finally = function(cb) {
  this.then(cb).catch(cb);
};