# Assignment 9
Cody Swartz

Assignment: Review NoSQL databases and their basic CRUD functionalities.

I implemented a UI under [./main/CRUD-tastic.html](./main/CRUD-tastic.html)

There is no static data in this code due to the UI.
The UI allows you to view DB info, add artists and songs, 
list artists, list songs for an artist, rename the artist 
with an "AKA", and delete the artist.

## Setup
- `npm install -g pouchdb-server`
- `npm install -g foreman`
- `npm install -g bower`
- `bower install`

## Run The DB Server
You have two options here, either run with foreman or run it directly.

After the server is up navigate to the `Fauxton UI` there is a link 
in the console or in the `./main/CRUD-tastic.html`

In Fauxton you'll need to set up a user account and enter the user//password
into `./main/CRUD-tastic.js` for the `options.auth`

It's also possible to go completely browser based and not use the server, but 
I did not test that route. You would use: `new PouchDB('music');` without 
any protocol to use the local browser DB.

### Run with Foreman
- `nf start`
  - The logs will tell you which available port was chosen for the PouchDB Server

### Run Individually
- `pouchdb-server --port 5000 --dir ./db --config ./db/config.json` the default port is normally 5984

## Open the Site
Open `./main/CRUD-tastic.html` enjoy!